import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { GithubComponent } from './github/github.component';
import { HttpClientModule } from "@angular/common/http";
import { UserComponent } from './user/user.component';


@NgModule({
  declarations: [
    AppComponent,
    GithubComponent,
    UserComponent
  ],
  imports: [
      BrowserModule,
      FormsModule,
      HttpClientModule,
      FormsModule,
      ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
