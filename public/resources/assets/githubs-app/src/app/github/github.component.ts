import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {HttpClient, HttpParams} from '@angular/common/http';
import { UserComponent } from '../user/user.component';

@Component({
  selector: 'app-github',
  templateUrl: './github.component.html',
  styleUrls: ['./github.component.css']
})
export class GithubComponent {
    user: UserComponent = new UserComponent();
    http: HttpClient;
    meuForm: FormGroup;
    result: Object;

    constructor (http: HttpClient, fb: FormBuilder) {
        this.http = http;

        this.meuForm = fb.group({
            user: ['', Validators.required]
        });
    }

    search() {
        event.preventDefault();
        const params = new HttpParams().set('user', this.user.user);

        this.http.get('http://192.168.99.100/github/users/', {params, responseType: 'json'}).subscribe(data => {
            this.user.avatar_url = (data.avatar_url);
            this.user.user = data.user;
            this.user.login = data.login;
            this.user.name = data.name;
        });
    }
}
