import { Component } from '@angular/core';
import { Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent {
    login: string = '';
    user: string = '';
    @Input() url: string = '';
    avatar_url: string = '';
    name: string = '';
    location: string = '';
    email: string = '';
}
