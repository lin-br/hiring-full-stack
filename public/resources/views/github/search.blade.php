<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>GithubsApp</title>
    <base href="/">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="favicon.ico">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<app-root>Carregando...</app-root>
<script type="text/javascript" src="js/inline.bundle.js"></script>
<script type="text/javascript" src="js/polyfills.bundle.js"></script>
<script type="text/javascript" src="js/styles.bundle.js"></script>
<script type="text/javascript" src="js/vendor.bundle.js"></script>
<script type="text/javascript" src="js/main.bundle.js"></script>
</body>
</html>
