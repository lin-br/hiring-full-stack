<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', function () {
    return view('github.search');
});

Route::get('/github/users/', ['middleware' => 'cors', 'as' => 'github/users', 'uses' => 'GitHubController@getUser']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
