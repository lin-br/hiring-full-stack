<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class GitHubController extends Controller
{
    public function getUser(Request $request)
    {
        $user = $request->query('user');

        $httpClient = new Client();

        $responseClient = $httpClient->get('https://api.github.com/users/' . $user)->getBody();

        $json = \GuzzleHttp\json_decode($responseClient);

        return response()->json($json);
    }
}
