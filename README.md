#Etapas para execução e visualização do projeto:
<br />
<br />
<p>
##-> Faça o download do repositório.
</p>
<p>
##-> Acesse o diretório hiring-full-stack, com o seu terminal favorito.
</p>
<p>
##-> Execute o comando no seu terminal: </br >
    `docker-compose up -d`
</p>
<p>
###-> Acesse o diretório public e execute o comando: </br >
    `composer install`
</p>
<p>
###-> Acesse o diretório resources/assets/githubs-app/ e execute o comando: </br >
    `npm install`
</p>
<p>
###-> Após efetuar toda a instalação utilize o comando: </br >
    `ng build --output-path=./../../../public/js`
</p>
<p>
###-> Após a execução do comando, acesse o endereço do seu docker-machine via navegador.
</p>